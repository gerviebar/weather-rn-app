import React from "react";
import { NavigationContainer} from "@react-navigation/native"
import { createStackNavigator} from "@react-navigation/stack"

import CurrentLocation from "./components/CurrentLocation"
import Weather from "./components/Weather"

const Stack = createStackNavigator()

export default function App() {

  const days = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    const monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];

    const date = new Date()
    const dayName = days[date.getDay()];
    const monthName = monthNames[date.getMonth()];
    const dateNum = date.getDate();
  
  return (
    <NavigationContainer>
      <Stack.Navigator >
        <Stack.Screen name="Weather App">
        {(props) => (<CurrentLocation {...props}
        days={days}
        monthNames={monthNames}
        date={date}
        dayName={dayName}
        monthName={monthName}
        dateNum={dateNum}
        />)}
        </Stack.Screen>
        <Stack.Screen name="Weather Forecast">
        {(props) => (<Weather {...props}
        days={days}
        monthNames={monthNames}
        date={date}
        dayName={dayName}
        monthName={monthName}
        dateNum={dateNum}
        />)}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>  
  );
}



