# Weather Mobile App

**Weather App** provides users with an up-to-date weather conditions based on their actual location. Users can also obtain a weather forecast for any city in the world with autocomplete feature as well as a 6-day forecast using OpenWeatherMap API and Geolocation API.

## Built with
* React Native
* Expo
* OpenWeatherMap API
* Google Autocomplete API

## Setup

To run this project, install it locally using npm:
```
1. npm install expo-cli --global
2. cd weather-react-native-app
3. expo start
```

Gervie Barczyk gerviebarczyk@gmail.com