import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';

import Search from './Search'

export default function CurrentLocation(props) {
    const [data, setData] = useState('')
    const [isLoading, setIsLoading] = useState(true)

    useEffect(()=> {
        if('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(position => {
                const { latitude, longitude } = position.coords;
                getWeather(latitude, longitude)
          });
      } else {
        alert("Geolocation not available");
      }
    }, [])

    getWeather = async (lat, lon) => {
        try {
            const response = await fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=f4b8568b8feb4937fff843032e1ab095&units=metric`)
            const resJson = await response.json()
            setData({
                location: resJson.name,
                temp: resJson.main.temp,
                feelsLike: resJson.main.feels_like,
                humidity: resJson.main.humidity,
                pressure: resJson.main.pressure,
                windSpeed: resJson.wind.speed,
                tempMax: resJson.main.temp_max,
                tempMin: resJson.main.temp_min,
                description: resJson.weather[0].description,
                icon: resJson.weather[0].icon
            })
            setIsLoading({isLoading: false})
            } catch(error) {
                console.log(error)
            }
            }

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <Search params={props.navigation.navigate} />
            </View>
            { isLoading && (
            <View style={styles.currentLocationContainer}>
                <Text style={styles.locationText}>{data.location}</Text>
                <Text style={styles.currentDate}>{props.dayName}, {props.dateNum} {props.monthName}</Text>    
                <View style={styles.weatherDetailsWrapper}>
                    <View style={styles.weatherInfo}>
                        <Image style={styles.weatherIcon} source={{ uri: `http://openweathermap.org/img/wn/${data.icon}@4x.png` }}/>                         
                    </View>
                    <View>
                        <Text style={styles.temperatureText}>{Math.round(data.temp)}°</Text>
                    </View> 
                </View> 
                    <Text style={styles.description}>Feels like {Math.round(data.feelsLike)}° | {data.description}</Text>
                    <Text style={styles.temp}>{Math.round(data.tempMin)}° / {Math.round(data.tempMax)}°</Text>
                    <View style={styles.row}>
                        <View>
                            <Text style={styles.tempLabel}>Humidity</Text>
                            <Text style={styles.tempLabel}>Wind</Text>
                            <Text style={styles.tempLabel}>Pressure</Text>
                        </View >                          
                        <View>
                            <Text style={styles.labelDetails}>{data.humidity}%</Text>
                            <Text style={styles.labelDetails}>{((data.windSpeed * 18)/5).toFixed()}km/hr</Text>
                            <Text style={styles.labelDetails}>{data.pressure}hPa</Text>
                        </View>
                    </View>     
            </View>
            )}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#EAF8FF",
      },
    currentLocationContainer: {
        flex: 2,
        marginTop: 60,
        margin: 15,
        backgroundColor: '#7CDCFE',
        borderRadius: 10,
    },
    weatherIcon: {
        height: 140,
        width: 120,
    },
    currentWeatherText: {
        fontSize: 18,
        color: '#3B738D',
        textAlign: 'left',
        marginLeft: 15,
        marginTop: 25,
    },
    locationText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'left',
        marginLeft: 15,
        marginTop: 20
    },
    temperatureText: {
        fontSize: 100,
        marginTop: 30,
        color: 'white',
        marginRight: 50,
    },
    weatherInfo: {
        marginTop: 20,
        alignItems: 'flex-start'
    },
    weatherDetailsWrapper: {
        flexDirection: 'row',
    },
    description: {
        fontSize: 18,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 40,
    },
    currentDate: {
        marginLeft: 15,
        color: 'white'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'center',
        marginTop: 50,
        width: '60%'
    },
    temp: {
        fontSize: 15, 
        color: '#3B738D',
        textAlign: 'center',
        marginTop: 10,
    },
    tempLabel: {
        color: '#7894BA',
        padding: 4
    },
    labelDetails: {
        color: '#3B738D',
        padding: 4
    }

})
