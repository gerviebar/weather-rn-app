import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native';
import WeatherDisplay from './WeatherDisplay';

export default function Weather (props) {  
    const [data, setData] = useState([])
    
    useEffect(()=> {
        getWeather()
    }, [])
    
    async function getWeather() {
        try {
        const city = props.route.params.item.description

        const response = await fetch(`https://api.openweathermap.org/data/2.5/forecast/daily?q=${city}&cnt=7&appid=16909a97489bed275d13dbdea4e01f59&units=metric`)
        const resJson = await response.json()
        setData([resJson])            
        } catch(error) {
            console.log(error)
        }
    }

    return (
        <View style={styles.container}> 
        {data.map((obj, i)=> {          
            return (<View key={i}>
                        <View style={styles.locationWrapper}>
                            <Text style={styles.locationText}>{obj.city.name}</Text>
                            <Text style={styles.currentDate}>{props.dayName}, {props.dateNum} {props.monthName}</Text>                          
                        </View>
                        <View style={styles.weatherInfo}>                          
                            <Text style={styles.temperature}>{Math.round(obj.list[0].temp.day)}°</Text>
                            <Text style={styles.weatherDescription}>{obj.list[0].weather[0].description}</Text>                                   
                            <Text style={styles.averageTemp}>{Math.round(obj.list[0].temp.min)}° / {Math.round(obj.list[0].temp.max)}°</Text>                                       
                        </View>
                        <View>
                          <WeatherDisplay listDt={obj.list} dayNames={props.days}/>  
                        </View> 
                    </View>
                )
        })}                 
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,       
        backgroundColor: '#7CDCFE'
    },
    locationWrapper:{
        top: 20,
        marginLeft: 20,
    }, 
    locationText: {
        fontSize: 18,      
        color: 'white',
    },
    currentDate: {
        color: 'white',
    },
    weatherInfo: {
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
    temperature: {
        color: 'white',
        fontSize: 90,
    },
    weatherDescription: {
        textTransform: 'capitalize',
        color: 'white',
        fontSize: 18,
    },
    averageTemp: {
        color: 'white',
    }
  });
