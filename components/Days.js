import React, { useState } from 'react'
import {StyleSheet, Text } from 'react-native';

export default function Days(props) {
    const [unixDt] = useState([props.unix])

    const timestamp = unixDt.map(el => {
        const date = new Date(el * 1000);
        const dayOfWeek = props.dayNames[date.getDay()]
        return dayOfWeek
    });                 
    
    return (  
            <Text style={styles.dayNames}>{timestamp}</Text>     
    )
}

const styles = StyleSheet.create({
    dayNames: {
        color: 'white'
    },
})
