import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native';
import Days from './Days'

export default function WeatherDisplay(props) { 

    return (
        <View>
            {props.listDt.slice(1).map((el,i)=> {
              return <View key={i} style={styles.weatherContainer}>
                        <View style={styles.background}>
                            <Days unix={el.dt} dayNames={props.dayNames} />
                        </View>
                        <View style={styles.background}>
                            <Text style={styles.tempText}>{Math.round(el.temp.night)}° / {Math.round(el.temp.day)}°</Text>
                        </View>
                        <View style={styles.background}>
                            <Image style={styles.weatherIcon} source={{ uri: `http://openweathermap.org/img/wn/${el.weather[0].icon}@4x.png` }}/>
                        </View>
                    </View>
            })}

        </View>
    )
}

const styles = StyleSheet.create({
    weatherContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        padding: 8,
    },
    weatherIcon: {
        height: 30,
        width: 30,
        alignSelf: 'flex-end',
    },
    tempText: {
        textAlign: 'center',
        color: 'white'
    },
    background: {
        flex: 1,
    },
})