import React from 'react';
import { View, Text, FlatList, TextInput, StyleSheet, SafeAreaView } from 'react-native';
import { TouchableOpacity} from 'react-native-gesture-handler'
import axios from 'axios';
export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchKeyword: '',
      searchResults: [],
      isShowingResults: false,
    };
  }

  searchLocation = async (text) => {
    this.setState({searchKeyword: text});
    axios
      .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyC5Z1iLTQnhAER0mGvqCv5Dx9dp-Wo673w&input=${this.state.searchKeyword}`,
      })
      .then((response) => {
        this.setState({
          searchResults: response.data.predictions,
          isShowingResults: true,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  };


  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View>
        <View style={styles.inputWrapper}>
          <TextInput
            placeholder="Search for a city..."
            returnKeyType="search"
            style={styles.input}
            placeholderTextColor="#000"
            onChangeText={(text) => this.searchLocation(text)}
            value={this.state.searchKeyword}
          />
          </View>
          {this.state.isShowingResults && (
            <FlatList
              style={styles.searchResultsContainer}
              data={this.state.searchResults}
              keyExtractor={(item) => item.place_id}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity style={styles.resultItem} 
                  onPress={()=>{
                    this.setState({
                      isShowingResults: false,
                    })
                    this.props.params('Weather Forecast', {item: item})}}>
                  <Text>{item.description}</Text>
                </TouchableOpacity>
                );
              }}
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center'
  },
  inputWrapper: {
    marginTop: 10,
    flexDirection: "row",
  },
  input: {
    height: 40,
    width: 330,
    borderWidth: 1.5,
    paddingLeft: 15,
    backgroundColor: '#7CDCFE',
    color: 'white',
    fontSize: 15,
    borderRadius: 12,
    borderColor: '#EAF8FF'
  },
  resultItem: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: 40,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    width: '100%'
  },
  searchResultsContainer: {
    height: 260,
    backgroundColor: '#fff',
    position: 'absolute',
    top: 60,
    zIndex: 1,
    marginRight: 30,
  },
})

